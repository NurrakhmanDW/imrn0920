/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 *
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 *
 * Selamat mengerjakan
 */

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor(points = 0, subject = "subject", email = " email@mail.com") {
    this._subject = subject;
    this._points = points;
    this._email = email;
  }
  average() {
    if (Array.isArray(this._points)) {
      return (
        this._points.reduce((prev, curr) => prev + curr) / this._points.length
      );
    } else if (!isNaN(this._points)) {
      return this._points;
    } else {
      return "Points is not a number";
    }
  }
  viewScores(subject) {
    switch (subject) {
      case "quiz-1":
        return {
          email: this._email,
          subject: this._subject,
          points: this._points[0]
        };
        break;
      case "quiz-2":
        return {
          email: this._email,
          subject: this._subject,
          points: this._points[1]
        };
        break;
      case "quiz-3":
        return {
          email: this._email,
          subject: this._subject,
          points: this._points[2]
        };
        break;

      default:
        return "subject not found";
        break;
    }
  }
}

var score = new Score([1, 2, 3, 4, 5, 6, 7]);
console.log(score.average());

/*=========================================== 
    2. SOAL Create Score (10 Poin + 5 Poin ES6)
    ===========================================
    Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
    Function viewScores mengolah data email dan nilai skor pada parameter array 
    lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
    Contoh: 
  
    Input
     
    data : 
    [
      ["email", "quiz-1", "quiz-2", "quiz-3"],
      ["abduh@mail.com", 78, 89, 90],
      ["khairun@mail.com", 95, 85, 88]
    ]
    subject: "quiz-1"
  
    Output 
    [
      {email: "abduh@mail.com", subject: "quiz-1", points: 78},
      {email: "khairun@mail.com", subject: "quiz-1", points: 95},
    ]
  */

const data = [
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
];

function viewScores(data, subject) {
  var classScore = [];
  data.map(student => {
    var email = student[0];
    var points = [student[1], student[2], student[3]];
    classScore.push(new Score(points, subject, email).viewScores(subject));
  });
  console.log(classScore);
}

// TEST CASE
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");

/*==========================================
    3. SOAL Recap Score (15 Poin + 5 Poin ES6)
    ==========================================
      Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
      Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
      predikat kelulusan ditentukan dari aturan berikut:
      nilai > 70 "participant"
      nilai > 80 "graduate"
      nilai > 90 "honour"
  
      output:
      1. Email: abduh@mail.com
      Rata-rata: 85.7
      Predikat: graduate
  
      2. Email: khairun@mail.com
      Rata-rata: 89.3
      Predikat: graduate
  
      3. Email: bondra@mail.com
      Rata-rata: 74.3
      Predikat: participant
  
      4. Email: regi@mail.com
      Rata-rata: 91
      Predikat: honour
  
  */

function recapScores(data) {
  data.map((student, index) => {
    let email = student[0];
    let points = [student[1], student[2], student[3]];
    let recapedScore = new Score(points, "subject", email);
    console.log(`${index + 1}. Email: ${recapedScore._email}`);
    console.log(`Rata-rata: ${recapedScore.average()}`);
    var peringkat = "failed";
    var average = recapedScore.average();
    if (parseFloat(average) > 90) {
      peringkat = "honour";
    } else if (parseFloat(average) > 80) {
      peringkat = "graduate";
    } else if (parseFloat(average) > 70) {
      peringkat = "participant";
    }
    console.log(`Predikat: ${peringkat}`);
  });
}

function viewScores(data, subject) {
  var classScore = [];
  data.map(student => {
    var email = student[0];
    var points = [student[1], student[2], student[3]];
    classScore.push(new Score(points, subject, email).viewScores(subject));
  });
  console.log(classScore);
}

recapScores(data);
