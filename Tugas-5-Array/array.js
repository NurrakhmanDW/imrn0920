// No. 1 (Range)
console.log("No. 1 (Range)");

function range(a, b) {
  if (typeof a === "undefined" || typeof b === "undefined") {
    return -1;
  } else if (a > b) {
    var tmp = [];
    for (let index = a; index >= b; index--) {
      tmp.push(index);
    }
    return tmp;
  } else if (a < b) {
    var tmp = [];
    for (let index = a; index <= b; index++) {
      tmp.push(index);
    }
    return tmp;
  } else {
    return [a];
  }
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// No. 2 (Range with Step)
console.log("\nNo. 2 (Range with Step)");

function rangeWithStep(a, b, c) {
  if (typeof a === "undefined" && typeof b === "undefined") {
    return -1;
  } else if (a > b) {
    var tmp = [];
    var step = c ? c : 1;
    for (let index = a; index >= b; ) {
      tmp.push(index);
      index = index - step;
    }
    return tmp;
  } else if (a < b) {
    var tmp = [];
    var step = c ? c : 1;
    for (let index = a; index <= b; ) {
      tmp.push(index);
      index = index + step;
    }
    return tmp;
  } else {
    return [a];
  }
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

// No. 3 (Sum of Range)
console.log("\nNo. 3 (Sum of Range)");

function sum(a, b, c) {
  tmp = rangeWithStep(a, b, c);
  var sum = 0;
  for (let index = 0; index < tmp.length; index++) {
    sum = parseInt(sum) + parseInt(tmp[index]);
  }
  return sum;
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

// No. 4 (Array Multidimensi)
console.log("\nNo. 4 (Array Multidimensi)");

//contoh input
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(data) {
  data.map(x => {
    console.log("\n");
    console.log("Nomor ID: " + x[0]);
    console.log("Nama Lengkap:: " + x[1]);
    console.log("TTL: " + x[2] + " " + x[3]);
    console.log("Hobi: " + x[4]);
  });
}
console.log(dataHandling(input));

// No. 5 (Balik Kata)
console.log("\nNo. 5 (Balik Kata)");

function balikKata(text) {
  var tmp = "";
  for (let index = text.length - 1; index >= 0; index--) {
    tmp = tmp + text[index];
  }
  return tmp;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

// No. 6 (Metode Array)
console.log("\nNo. 6 (Metode Array)");

function dataHandling2(data) {
  //splice
  data.splice(4, 1, "Pria", "SMA Internasional Metro");
  data.splice(1, 1, data[1] + " Elsharawy");
  data.splice(2, 1, "Provinsi " + data[2]);
  console.log(data);

  //split
  var date = data[3].split("/");
  switch (parseInt(date[1])) {
    case 01:
      console.log("Januari");
      break;
    case 02:
      console.log("Febuari");
      break;
    case 03:
      console.log("Maret");
      break;
    case 04:
      console.log("April");
      break;
    case 05:
      console.log("Mei");
      break;
    case 06:
      console.log("Juni");
      break;
    case 07:
      console.log("Juli");
      break;
    case 08:
      console.log("Agustus");
      break;
    case 09:
      console.log("September");
      break;
    case 10:
      console.log("Oktober");
      break;
    case 11:
      console.log("November");
      break;
    case 12:
      console.log("Desember");
      break;
    default:
      break;
  }
  
  //sort
  var sortDate = data[3].split("/");;
  sortDate.sort(function (value1, value2) { return value2 - value1 } ) ;
  console.log(sortDate);

  //join
  var joinDate = data[3].split("/");
  joinDate.join("-")
  console.log(joinDate)
  
  //slice
  var name = data[1].slice(0,15)
  console.log(name)
}

var input = [
  "0001",
  "Roman Alamsyah",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca"
];
dataHandling2(input);
