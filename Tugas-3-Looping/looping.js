// No.1 (Looping While)
console.log("\nNo.1 (Looping While)");

var a = 0;
var flag = false;
var isFirstLoop = true;

console.log("LOOPING PERTAMA");
while (!flag) {
  if (isFirstLoop) {
    a = a + 2;
    console.log(a + " - I love coding");
    if (a >= 20) {
      isFirstLoop = false;
      console.log("LOOPING KEDUA");
    }
  } else {
    console.log(a + " - I will become a mobile developer");
    a = a - 2;
    if (a <= 0) {
      flag = true;
    }
  }
}

// No.2 (Looping For)
console.log("\nNo.2 (Looping For)");

for (let index = 1; index <= 20; index++) {
  if (index % 3 == 0 && index % 2 == 1) {
    console.log(index + " - I Love Coding");
  } else if (index % 2 == 1) {
    console.log(index + " - Santai");
  } else if (index % 2 === 0) {
    console.log(index + " - Berkualitas");
  }
}

// No.3 (Membuat Persegi Panjang)
console.log("\nNo.3 (Membuat Persegi Panjang)");

var x = 8;
var y = 4;

for (var i = 1; i <= y; i++) {
  for (var j = 1; j <= x; j++) {
    process.stdout.write("#");
  }
  process.stdout.write("\n");
}

// No.4 (Membuat Tangga)
console.log("\nNo.4 (Membuat Tangga)");

var alasNtinggi = 7;

for (var i = 1; i <= alasNtinggi; i++) {
  for (var j = 1; j <= i; j++) {
    process.stdout.write("#");
  }
  process.stdout.write("\n");
}

// No.5 (Membuat Persegi Panjang)
console.log("\nNo.5 (Membuat Papan Catur)");

var x = 8;
var y = 8;

for (var i = 1; i <= y; i++) {
  for (var j = 1; j <= x; j++) {
    if ((i + j) % 2 == 0) {
        process.stdout.write(" ");
    } else{
        process.stdout.write("#");
    }
  }
  process.stdout.write("\n");
}
