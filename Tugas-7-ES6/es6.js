// No. 1 (Mengubah fungsi menjadi fungsi arrow)
console.log("No. 1 (Mengubah fungsi menjadi fungsi arrow)");

const golden = () => {
  console.log("this is golden!!");
};

golden();

// No.2 (Sederhanakan menjadi Object literal di ES6)
console.log("\nNo.2 (Sederhanakan menjadi Object literal di ES6)");

const newFunction = function literal(firstName, lastName) {
  return {
    firstName,
    lastName,
    fullName: function() {
      console.log(`${firstName} ${lastName}`);
      return;
    }
  };
};

newFunction("William", "Imoh").fullName();

// No.3 (Destructuring)
console.log("\nNo.3 (Destructuring)");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
};

const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation);

// No.4 (Array Spreading)
console.log("\nNo.4 (Array Spreading)");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west, ...east]

console.log(combined);

// No.5 (Template Literals)
console.log("\nNo.5 (Template Literals)");
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;

console.log(before);
