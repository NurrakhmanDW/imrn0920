// No. 1 (Array to Object)
console.log("No. 1 (Array to Object)");

var now = new Date();
var thisYear = now.getFullYear();

function arrayToObject(arr) {
  arr.map((people, index) => {
    var data;
    var age;
    if (people[3] != null) {
      if (people[3] < thisYear) {
        age = parseInt(thisYear) - parseInt(people[3]);
      } else {
        age = "Invalid Birth Year";
      }
    } else {
      age = "Invalid Birth Year";
    }
    data = {
      firstName: people[0],
      lastName: people[1],
      gender: people[2],
      age: age
    };
    console.log(
      index +
        1 +
        ". " +
        people[0] +
        " " +
        people[1] +
        ": " +
        JSON.stringify(data)
    );
  });
}

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"]
];
arrayToObject(people);

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023]
];
arrayToObject(people2);

// No. 2 (Shopping Time)
console.log("\nNo. 2 (Shopping Time)");

var catalog = [
  { name: "Sepatu Stacattu", price: 1500000 },
  { name: "Baju Zoro ", price: 500000 },
  { name: "Baju H&N", price: 250000 },
  { name: "Sweater Uniklooh ", price: 175000 },
  { name: "Casing Handphone ", price: 50000 }
];

//sort harga terbesar > terkecil
catalog.sort((a, b) => parseInt(b.price) - parseInt(a.price));

function shoppingTime(memberId, money) {
  if (typeof memberId === "undefined" || memberId === "") {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }
  if (money === "" || money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  }
  var shoppingList = [];
  var currMoney = money;
  catalog.map(x => {
    if (currMoney >= x.price) {
      currMoney = currMoney - x.price;
      shoppingList.push(x.name);
    }
  });
  var data = {
    memberId: "memberId",
    money: money,
    listPurchased: shoppingList,
    changeMoney: currMoney
  };
  return data;
}

// TEST CASES

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// No. 3 (Naik Angkot)
console.log("\nNo. 3 (Naik Angkot)");

function naikAngkot(arrPenumpang) {
  var rute = ["A", "B", "C", "D", "E", "F"];
  var data = [];
  arrPenumpang.map(penumpang => {
    var indexAwal = rute.findIndex(s => s === penumpang[1]);
    var indexAhir = rute.findIndex(s => s === penumpang[2]);
    var ongkos = (parseInt(indexAhir) - parseInt(indexAwal)) * 2000;
    var tmp = {
      penumpang: penumpang[0],
      naikDari: penumpang[1],
      tujuan: penumpang[2],
      bayar: ongkos
    };
    data.push(tmp);
  });
  return data;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"]
  ])
);
console.log(naikAngkot([])); //[]
