// No.1
console.log("No.1");

function teriak() {
  return "Halo Sanbers!";
}
console.log(teriak());

// No.2
console.log("\nNo.2");

function kalikan(a, b) {
  return parseInt(a) + parseInt(b);
}

var num1 = 12;
var num2 = 4;
console.log(kalikan(num1, num2));

// No.3
console.log("\nNo.3");

function introduce(name, age, address, body) {
  return (
    "Nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    address +
    ", dan saya punya hobby yaitu " +
    hobby +
    "!"
  );
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
