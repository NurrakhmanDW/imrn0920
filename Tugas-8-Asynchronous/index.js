// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 }
];

function readAgain(timespent, index) {
  if (index >= books.length) {
    return;
  }
  readBooks(timespent, books[index], timeleft =>
    readAgain(timeleft, index + 1)
  );
}

readAgain(10000, 0);