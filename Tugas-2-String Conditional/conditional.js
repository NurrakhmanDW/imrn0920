// Soal 1 (if-else)
console.log("-- Soal 1 (if-else)")

var nama = "John";
var peran = "";

if (nama !== "") {
  if (peran !== "") {
    console.log("Selamat Datang di Dunia Werewolf, " + nama);
    if (peran === "Penyihir") {
      console.log(
        "Halo Penyihir " +
          nama +
          ", kamu dapat melihat siapa yang menjadi werewolf!"
      );
    } else if (peran === "Guard") {
      console.log(
        "Halo Guard " +
          nama +
          ", kamu akan membantu melindungi temanmu dari serangan werewolf."
      );
    } else if (peran === "Werewolf") {
      console.log(
        "Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!"
      );
    }
  } else {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
  }
} else {
  console.log("Nama harus diisi!");
}

// Soal 2 (Switch Case)
console.log("-- Soal 2 (Switch Case)")

var tanggal = 20; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 5; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1996; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if (parseInt(tanggal) >= 1 && parseInt(tanggal) <= 31) {
  if (parseInt(bulan) >= 1 && parseInt(bulan) <= 12) {
    if (parseInt(tahun) >= 1900 && parseInt(tahun) <= 2200) {
      switch (bulan) {
        case 1:
          console.log(tanggal + " Januari " + tahun);
          break;
        case 2:
          console.log(tanggal + " Febuari " + tahun);
          break;
        case 3:
          console.log(tanggal + " Maret " + tahun);
          break;
        case 4:
          console.log(tanggal + " April " + tahun);
          break;
        case 5:
          console.log(tanggal + " Mei " + tahun);
          break;
        case 6:
          console.log(tanggal + " Juni " + tahun);
          break;
        case 7:
          console.log(tanggal + " Juli " + tahun);
          break;
        case 8:
          console.log(tanggal + " Agustus " + tahun);
          break;
        case 9:
          console.log(tanggal + " September " + tahun);
          break;
        case 10:
          console.log(tanggal + " Oktober " + tahun);
          break;
        case 11:
          console.log(tanggal + " November " + tahun);
          break;
        case 12:
          console.log(tanggal + " Desember " + tahun);
          break;
        default:
          break;
      }
    } else {
      console.log("Nilai variable Tahun di antara 1900 - 2200");
    }
  } else {
    console.log("Nilai variable Bulan di antara 1 - 12");
  }
} else {
  console.log("Nilai variable Tanggal di antara 1 - 31");
}
