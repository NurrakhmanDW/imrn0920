var sayHello = "Hello World!"
console.log(sayHello)

var name = "John"
var angka = 12
var todayisFriday = false

console.log(name)
console.log(angka)
console.log(todayisFriday)

var angka = 8
console.log(angka == "8") // true, padahal "8" adalah string.
console.log(angka === "8") // false, karena tipe data nya berbeda
console.log(angka === 8) // true 